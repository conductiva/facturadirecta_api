<?php

$apiBaseURL = "https://%s.facturadirecta.com";

function downloadInvoice($id, $accountName, $token, $filePath) {
	global $apiBaseURL;

	$url = sprintf($apiBaseURL, $accountName) . "/api/invoices/".$id.".pdf";

	$file = fopen ($filePath . "invoice_".$id.".pdf", "w+");

	$handler = curl_init($url);
	curl_setopt($handler, CURLOPT_TIMEOUT, 50);
	curl_setopt($handler, CURLOPT_FILE, $file);
	curl_setopt($handler, CURLOPT_FOLLOWLOCATION, true);
	curl_setopt($handler, CURLOPT_HEADER, 1);
	curl_setopt($handler, CURLOPT_USERPWD, $token.':x');

	curl_exec($handler);

	return $file;
}

/*
$file = downloadInvoice('invoice_ID', 'accont_name', 'api_token', './');

$meta_data = stream_get_meta_data($file);
$filename = $meta_data["uri"];
echo "Invoice downloaded at ".$filename."\n";
*/
?>