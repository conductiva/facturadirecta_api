<?php
function getProvidersFD($token, $cuenta){
	echo $url = "https://".$cuenta.".facturadirecta.com/api/providers.xml?api_token=".$token; 
	$handler = curl_init($url);
	curl_setopt($handler, CURLOPT_RETURNTRANSFER, true);  
	curl_setopt($handler, CURLOPT_TIMEOUT, 30);   
	curl_setopt($handler, CURLOPT_FOLLOWLOCATION, false);
	$response = curl_exec($handler);

	$doc = new DomDocument();
	$doc->loadXML($response);

	curl_close($handler);

	//foreach ($doc->childNodes as $node){
    //    print $node->nodeName.':'.$node->nodeValue;
    //}   

    return $doc;
		
}
?>
