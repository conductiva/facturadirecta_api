<?php

public static function insertarClienteFD($token, $cuenta, $clienteRes){
		
	$xml  = "<?xml version='1.0' encoding='UTF-8'?>";
	$xml .= "<client>";
	$xml .= "<legalType><![CDATA[".$clienteRes['tipo']."]]></legalType>";
	$xml .= "<personName><![CDATA[".$clienteRes['nombre']."]]></personName>";
	$xml .= "<personSurname><![CDATA[".$clienteRes['apellidos']."]]></personSurname>";
	$xml .= "<taxCode><![CDATA[".$clienteRes['nif']."]]></taxCode>";
	$xml .= "<address>";
	$xml .= "<line1><![CDATA[".$clienteRes['direccion']."]]></line1>";
	$xml .= "<city><![CDATA[".$clienteRes['ciudad']."]]></city>";
	$xml .= "<province><![CDATA[".$clienteRes['provincia']."]]></province>";
	$xml .= "<zipcode><![CDATA[".$clienteRes['cpostal']."]]></zipcode>";
	$xml .= "<country><![CDATA[".$clienteRes['pais']."]]></country>";
	$xml .= "</address>";
	$xml .= "<language><![CDATA[".$clienteRes['idioma']."]]></language>";
	$xml .= "<email><![CDATA[".$clienteRes['email']."]]></email>";
	$xml .= "<phone><![CDATA[".$clienteRes['telefono']."]]></phone>";
	$xml .= "<mobilePhone><![CDATA[".$clienteRes['movil']."]]></mobilePhone>";
	$xml .= "</client>";

	$url = "https://".$cuenta.".facturadirecta.com/api/clients.xml?api_token=".$token; 
			
	$handler = curl_init($url);
	curl_setopt($handler, CURLOPT_POST, 1);
	curl_setopt($handler, CURLOPT_HTTPHEADER, Array("Content-Type: text/xml"));
	curl_setopt($handler, CURLOPT_RETURNTRANSFER, true);  
	curl_setopt($handler, CURLOPT_TIMEOUT, 30);   
	curl_setopt($handler, CURLOPT_POSTFIELDS, $xml);
	curl_setopt($handler, CURLOPT_FOLLOWLOCATION, false);
	$response = curl_exec($handler);
		
	$doc = new DomDocument();
	$doc->loadXML($response);
		
	curl_close($handler);
	
	$itemId = $doc->getElementsByTagName("id")->item(0)->nodeValue;
	$httpStatus = $doc->getElementsByTagName("httpStatus")->item(0)->nodeValue;
		
	if(!empty($httpStatus)){
		$returnValue = '0';
	}else{
		$returnValue = $itemId;
	}
	
	return $returnValue;
		
}
?>